﻿using NAND_Prog;
using System.ComponentModel.Composition;

namespace ReadStatus_70h
{
    [Export(typeof(Operation)),
        ExportMetadata("Name", "ReadStatus_70h")]
    public class Read : AbstractRead
    {
        public Read()
        {
            name = "Read";
            based = typeof(Register);

            Instruction instruction;
            //--------------------------------------------------------------
            instruction = new WriteCommand();                              //Створюю інструкцію WriteCommand
            chainInstructions.Add(instruction);                            //Додаю її в ланцюжок інструкцій операції Read

            //    instruction.numberOfcycles = 0x01;                           //По дефолту 1       
            (instruction as WriteCommand).command = 0x70;
            (instruction as WriteCommand).Implementation = GetCommandMG;
            //--------------------------------------------------------------                   
            //--------------------------------------------------------------

            instruction = new ReadData();
            chainInstructions.Add(instruction);
            (instruction as ReadData).Implementation = GetDataMG;
            //--------------------------------------------------------------
        }

        private BufMG GetDataMG(ChipPart client)                        //Реалізація _implementation для  ReadData : Instruction
        {
            DataMG meneger = new DataMG();
            meneger.direction = Direction.In;    //дані на вхід

            meneger.SetDataPlace((client as IDatable).GetDataPlace());
            meneger.numberOfCycles = ((client as IDatable).GetDataSize());
            return meneger;

        }

    }
}
